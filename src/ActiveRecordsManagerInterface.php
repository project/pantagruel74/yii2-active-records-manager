<?php

namespace Pantagruel74\Yii2ActiveRecordsManager;

use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

interface ActiveRecordsManagerInterface
{
    /**
     * @template T
     * @param class-string|T $arInterface
     * @param Array<string|float|int|array> $properties
     * @return ActiveRecordInterface|T
     * @phpstan-ignore-next-line
     */
    public function create(string $arInterface, array $properties = []): ActiveRecordInterface;

    /**
     * @template T
     * @param class-string<T> $arInterface
     * @return ActiveQueryInterface
     */
    public function find(string $arInterface): ActiveQueryInterface;

    /**
     * @template T
     * @param class-string|T $arInterface
     * @param Array<string|float|int|array> $properties
     * @return ActiveRecordInterface|T
     * @phpstan-ignore-next-line
     */
    public function saveNew(string $arInterface, array $properties = []): ActiveRecordInterface;

    /**
     * @template T
     * @param class-string<T> $arInterface
     * @return class-string<T>
     */
    public function getClass(string $arInterface): string;

    /**
     * @template T
     * @param class-string<T> $arInterface
     * @return void
     */
    public function removeTableData(string $arInterface): void;

    /**
     * @return void
     */
    public function removeAllData(): void;
}