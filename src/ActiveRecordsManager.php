<?php

namespace Pantagruel74\Yii2ActiveRecordsManager;

use Webmozart\Assert\Assert;
use yii\base\Component;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class ActiveRecordsManager extends Component implements ActiveRecordsManagerInterface
{
    /* @var <class-string|ActiveRecordInterface>[] $map */
    /* @phpstan-ignore-next-line */
    public array $map = [];

    /**
     * @return void
     */
    public function init(): void
    {
        Assert::allSubclassOf(array_keys($this->map), ActiveRecordInterface::class);
        Assert::allSubclassOf(array_values($this->map), ActiveRecordInterface::class);
        Assert::allStringNotEmpty(array_keys($this->map));
        Assert::allStringNotEmpty(array_values($this->map));
        parent::init();
    }

    /**
     * @template T
     * @param class-string|T $arInterface
     * @param Array<string|float|int|array> $properties
     * @return ActiveRecordInterface|T
     * @phpstan-ignore-next-line
     */
    public function create(string $arInterface, array $properties = []): ActiveRecordInterface
    {
        Assert::notEmpty($this->map[$arInterface]);
        $targetClass = $this->map[$arInterface];
        /* @var class-string|ActiveRecordInterface $targetClass */
        /* @phpstan-ignore-next-line */
        return new $targetClass($properties);
    }

    /**
     * @template T
     * @param class-string<T> $arInterface
     * @return ActiveQueryInterface
     */
    public function find(string $arInterface): ActiveQueryInterface
    {
        Assert::notEmpty($this->map[$arInterface]);
        $targetClass = $this->map[$arInterface];
        /* @var class-string|ActiveRecordInterface $targetClass */
        return $targetClass::find();
    }

    /**
     * @template T
     * @param class-string|T $arInterface
     * @param Array<string|float|int|array> $properties
     * @return ActiveRecordInterface|T
     * @phpstan-ignore-next-line
     */
    public function saveNew(string $arInterface, array $properties = []): ActiveRecordInterface
    {
        $ar = $this->create($arInterface, $properties);
        /* @var T $ar */
        $ar->save();
        return $ar;
    }

    /**
     * @template T
     * @param class-string<T> $arInterface
     * @return class-string<T>
     */
    public function getClass(string $arInterface): string
    {
        Assert::notNull($this->map[$arInterface]);
        return $this->map[$arInterface];
    }

    /**
     * @template T
     * @param class-string<T> $arInterface
     * @return void
     */
    public function removeTableData(string $arInterface): void
    {
        Assert::notEmpty($this->map[$arInterface]);
        $targetClass = $this->map[$arInterface];
        /* @var class-string|ActiveRecordInterface $targetClass */
       foreach ($targetClass::find()->all() as $item)
       {
           $item->delete();
       }
    }

    /**
     * @return void
     */
    public function removeAllData(): void
    {
        foreach ($this->map as $recClassInterface => $recClass)
        {
            $this->removeTableData($recClassInterface);
        }
    }

}