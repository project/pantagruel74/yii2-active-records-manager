<?php

namespace Pantagruel74\Yii2ActiveRecordsManagerTetsUnit;

use Pantagruel74\Yii2ActiveRecordsManager\ActiveRecordsManager;
use Pantagruel74\Yii2ActiveRecordsManagerStubs\MiniActiveRecord;
use Pantagruel74\Yii2ActiveRecordsManagerStubs\MiniActiveRecordInterface;
use Pantagruel74\Yii2ActiveRecordsManagerStubs\SuperActiveRecord;
use Pantagruel74\Yii2ActiveRecordsManagerStubs\SuperActiveRecordInterface;
use Pantagruel74\Yii2ActiveRecordsManagerStubs\SuperQuery;
use Pantagruel74\Yii2Loader\Yii2Loader;
use PHPUnit\Framework\TestCase;
use Webmozart\Assert\Assert;

class ActiveRecordManagerTest extends TestCase
{
    public function testActiveRecordManager(): void
    {
        Yii2Loader::load();
        $manager = new ActiveRecordsManager([
            'map' => [
                MiniActiveRecordInterface::class => MiniActiveRecord::class,
                SuperActiveRecordInterface::class => SuperActiveRecord::class,
            ],
        ]);
        $miniAr = $manager->create(MiniActiveRecordInterface::class, [
            'alpha' => 'omega',
        ]);
        Assert::isAOf($miniAr, MiniActiveRecord::class);
        $this->assertEquals('omega', $miniAr->alpha);
        /* @var MiniActiveRecord $miniAr */
        $this->assertEquals(MiniActiveRecordInterface::MINIMSG, $miniAr->mini());

        $supQuery = $manager->find(SuperActiveRecordInterface::class);
        $this->assertEquals(SuperQuery::class, get_class($supQuery));

        $this->assertEquals(MiniActiveRecord::class, $manager->getClass(MiniActiveRecordInterface::class));
    }
}