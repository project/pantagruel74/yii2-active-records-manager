<?php

namespace Pantagruel74\Yii2ActiveRecordsManagerStubs;

use yii\db\ActiveRecordInterface;

interface MiniActiveRecordInterface extends ActiveRecordInterface
{
    const MINIMSG = "Minimini!";

    public function mini(): string;
}