<?php

namespace Pantagruel74\Yii2ActiveRecordsManagerStubs;

use yii\db\ActiveRecordInterface;

interface SuperActiveRecordInterface extends ActiveRecordInterface
{
    const SUPERMSG = "superSuper!";

    public function super(): string;
}