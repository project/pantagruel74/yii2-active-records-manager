<?php

namespace Pantagruel74\Yii2ActiveRecordsManagerStubs;

use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;
use yii\db\Connection;
use yii\db\QueryInterface;

class SuperQuery implements ActiveQueryInterface
{
    public function asArray($value = true)
    {
        throw new \Error("Method is abstract");
    }

    public function one($db = null)
    {
        throw new \Error("Method is abstract");
    }

    public function indexBy($column)
    {
        throw new \Error("Method is abstract");
    }

    public function with()
    {
        throw new \Error("Method is abstract");
    }

    public function via($relationName, callable $callable = null)
    {
        throw new \Error("Method is abstract");
    }

    public function findFor($name, $model)
    {
        throw new \Error("Method is abstract");
    }

    public function all($db = null)
    {
        throw new \Error("Method is abstract");
    }

    public function count($q = '*', $db = null)
    {
        throw new \Error("Method is abstract");
    }

    public function exists($db = null)
    {
        throw new \Error("Method is abstract");
    }

    public function where($condition)
    {
        throw new \Error("Method is abstract");
    }

    public function andWhere($condition)
    {
        throw new \Error("Method is abstract");
    }

    public function orWhere($condition)
    {
        throw new \Error("Method is abstract");
    }

    public function filterWhere(array $condition)
    {
        throw new \Error("Method is abstract");
    }

    public function andFilterWhere(array $condition)
    {
        throw new \Error("Method is abstract");
    }

    public function orFilterWhere(array $condition)
    {
        throw new \Error("Method is abstract");
    }

    public function orderBy($columns)
    {
        throw new \Error("Method is abstract");
    }

    public function addOrderBy($columns)
    {
        throw new \Error("Method is abstract");
    }

    public function limit($limit)
    {
        throw new \Error("Method is abstract");
    }

    public function offset($offset)
    {
        throw new \Error("Method is abstract");
    }

    public function emulateExecution($value = true)
    {
        throw new \Error("Method is abstract");
    }
}