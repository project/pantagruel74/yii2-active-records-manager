<?php

namespace Pantagruel74\Yii2ActiveRecordsManagerStubs;

use yii\base\Model;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;

class MiniActiveRecord extends Model implements MiniActiveRecordInterface
{
    public string $alpha;

    public function mini(): string
    {
        return self::MINIMSG;
    }

    public static function primaryKey()
    {
        throw new \Error("Method is abstract");
    }

    public function getAttribute($name)
    {
        throw new \Error("Method is abstract");
    }

    public function setAttribute($name, $value)
    {
        throw new \Error("Method is abstract");
    }

    public function hasAttribute($name)
    {
        throw new \Error("Method is abstract");
    }

    public function getPrimaryKey($asArray = false)
    {
        throw new \Error("Method is abstract");
    }

    public function getOldPrimaryKey($asArray = false)
    {
        throw new \Error("Method is abstract");
    }

    public static function isPrimaryKey($keys)
    {
        throw new \Error("Method is abstract");
    }

    public static function find()
    {
        throw new \Error("Method is abstract");
    }

    public static function findOne($condition)
    {
        throw new \Error("Method is abstract");
    }

    public static function findAll($condition)
    {
        throw new \Error("Method is abstract");
    }

    public static function updateAll($attributes, $condition = null)
    {
        throw new \Error("Method is abstract");
    }

    public static function deleteAll($condition = null)
    {
        throw new \Error("Method is abstract");
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        throw new \Error("Method is abstract");
    }

    public function insert($runValidation = true, $attributes = null)
    {
        throw new \Error("Method is abstract");
    }

    public function update($runValidation = true, $attributeNames = null)
    {
        throw new \Error("Method is abstract");
    }

    public function delete()
    {
        throw new \Error("Method is abstract");
    }

    public function getIsNewRecord()
    {
        throw new \Error("Method is abstract");
    }

    public function equals($record)
    {
        throw new \Error("Method is abstract");
    }

    public function getRelation($name, $throwException = true)
    {
        throw new \Error("Method is abstract");
    }

    public function populateRelation($name, $records)
    {
        throw new \Error("Method is abstract");
    }

    public function link($name, $model, $extraColumns = [])
    {
        throw new \Error("Method is abstract");
    }

    public function unlink($name, $model, $delete = false)
    {
        throw new \Error("Method is abstract");
    }

    public static function getDb()
    {
        throw new \Error("Method is abstract");
    }
}